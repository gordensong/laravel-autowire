<?php

namespace Tests;

use GordenSong\Laravel\Support\Autowire;

#[Autowire]
class Human
{
	#[Autowire]
	public ?Head $head = null;
}