<?php

use GordenSong\Laravel\Support\AutowireHelper;
use Illuminate\Contracts\Container\BindingResolutionException;

if (!function_exists('autowire')) {
	/**
	 * <p>
	 * 非递归：只实例化当前类及初始化当前类的注解属性
	 * <p>
	 * 递归：实例话所有注解类及类下的注解属性
	 * @throws ReflectionException
	 * @throws BindingResolutionException
	 */
	function autowire(string $class, bool $recursive = false)
	{
		return AutowireHelper::make($class, $recursive);
	}
}

if (!function_exists('autowireProperties')) {
	/**
	 * 默认只实例话当前类的注解属性
	 * @throws ReflectionException
	 * @throws BindingResolutionException
	 */
	function autowireProperties(object $object, bool $recursive = false)
	{
		AutowireHelper::autowireProperties($object, $recursive);
	}
}
