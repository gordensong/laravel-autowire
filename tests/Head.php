<?php

namespace Tests;

use GordenSong\Laravel\Support\Autowire;

#[Autowire]
class Head
{
	#[Autowire]
	public ?Mouth $mouth = null;
}