<?php

namespace Tests;

use GordenSong\Laravel\Support\Autowire;
use GordenSong\Laravel\Support\AutowireTrait;

class NotRecursiveAutowireTest extends TestCase
{
	// 第一步：引用 Trait use AutowireTrait
	use AutowireTrait;

	protected function setUp(): void
	{
		parent::setUp();

		// 第二步： $this->autowireProperties(); 绑定值为空的属性
		$this->autowireProperties(); // 不递归绑定
	}

	// 第三步：使用注解
	#[Autowire]
	private ?Human $human = null;

	public function test_auto_wire_head_is_null()
	{
		self::assertNotNull($this->human);
		self::assertNull($this->human->head);
	}
}