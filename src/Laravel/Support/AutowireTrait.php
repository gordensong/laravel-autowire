<?php

namespace GordenSong\Laravel\Support;

use Illuminate\Contracts\Container\BindingResolutionException;
use ReflectionException;

trait AutowireTrait
{
	/**
	 * @throws ReflectionException
	 * @throws BindingResolutionException
	 */
	protected function autowireProperties(bool $recursive = false): void
	{
		autowireProperties($this, $recursive);
	}
}