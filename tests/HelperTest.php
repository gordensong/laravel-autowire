<?php

namespace Tests;

class HelperTest extends TestCase
{
	public function test_autowire()
	{
		/** @var Human $cat */
		$cat = autowire(Human::class);

		self::assertNotNull($cat);
		self::assertNotNull($cat->head);
		self::assertNull($cat->head->mouth);
	}

	public function test_autowire_recursive()
	{
		/** @var Human $cat */
		$cat = autowire(Human::class, true);

		self::assertNotNull($cat);
		self::assertNotNull($cat->head);
		self::assertNotNull($cat->head->mouth);
		self::assertNull($cat->head->mouth->teeth);
	}
}