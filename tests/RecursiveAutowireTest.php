<?php

namespace Tests;

use GordenSong\Laravel\Support\Autowire;
use GordenSong\Laravel\Support\AutowireTrait;

class RecursiveAutowireTest extends TestCase
{
	use AutowireTrait; // 1.

	protected function setUp(): void
	{
		parent::setUp();

		$this->autowireProperties(true); // 2. 递归绑定
	}

	// 3. 使用注解
	#[Autowire]
	private ?Human $cat = null;

	public function test_autowire_head_is_null()
	{
		self::assertNotNull($this->cat);
		self::assertNotNull($this->cat->head);
		self::assertNotNull($this->cat->head->mouth);
		self::assertNull($this->cat->head->mouth->teeth); // teeth 未加注解
	}
}